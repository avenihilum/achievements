<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/achievementsFrame.css">
	</head>

	<body>
		<?php
			include('connection.php');

			$query = mysqli_query($connection, "SELECT name, description, points, image FROM achievements");

			while ($row = mysqli_fetch_assoc($query)) {			
				echo "<div class='achievementResult'>
						<div id='levi'>
							<img src='". $row['image'] ."' />
						</div>

						<div id='desni'>
							<div class='achievementPoints'>". $row['points'] ." poena</div>
							<div class='achievementName'>". $row['name'] ."</div>
							<div class='achievementDesc'>". $row['description'] ."</div>
						</div>

						<div id='achievementResultBackground'><img src='../images/site/achievementResultShade.png' /></div>
					 </div>";
			}
		?>
	</body>
</html>