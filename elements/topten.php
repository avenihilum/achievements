<?php
	include("connection.php");
	$query = mysqli_query($connection, "SELECT username, image, frameImage, points FROM users ORDER BY points DESC LIMIT 10");

	while ($row = mysqli_fetch_assoc($query)) {			
		echo "<div class='topUser'>
				<div class='toptenUserPoints'>".$row['points']." poena.</div>
				<img class='toptenUserAvatarImage' src='".$row['image']."' />
				<img class='toptenUserAvatarFrame' src='".$row['frameImage']."' />
				<div class='toptenUserUsername'>".$row['username']."</div>
			</div>";
	};
?>
