<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/viewAchievementsFrame.css">
	</head>

	<body>
		<?php
			include('connection.php');
			session_start();
				$user = $_SESSION['user'];
				echo $user;
				$query = mysqli_query($connection, "SELECT achievements, points FROM users WHERE username='$user'");
				$row = mysqli_fetch_assoc($query);
				echo "<div id='achievementPoints'>". $row['points'] ."</div>";

				$achievements = $row['achievements'];
				$achs = explode(",", $achievements);
				$achsFinal = [];

				$cnt = 0;
				for ($i=0; $i<count($achs); $i++) {
					$string = str_replace(",", "", $achs[$i]);
					if ($string !== "") {
						$achsFinal[$cnt] = $string;
						$cnt++;
					};
				};

				for ($i=0; $i<count($achsFinal); $i++) {
					$query2 = mysqli_query($connection, "SELECT name, description, points, image FROM achievements 
						WHERE id='$achsFinal[$i]'");
					$data = mysqli_fetch_assoc($query2);

					echo "<div class='achievementResult'>
							<div id='levi'>
								<img src='". $data['image'] ."' />
							</div>

							<div id='desni'>
								<div class='achievementPoints'>". $data['points'] ." poena</div>
								<div class='achievementName'>". $data['name'] ."</div>
								<div class='achievementDesc'>". $data['description'] ."</div>
							</div>

							<div id='achievementResultBackground'><img src='../images/site/achievementResultShade.png' /></div>
						 </div>";
				};
			session_commit();
		?>
	</body>
</html>