<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/avatarChange.css" />
		<script src="../scripts/jquery-3.1.0.min.js"></script>
	</head>

	<body>
		<?php
			$i=1;
			$dir = "../images/profile/avatars/";
			$files = scandir($dir);
			$brojAvatara = (count($files)-2)/2;

			while ($i <= $brojAvatara) {
				echo "<img class='newAvatar' src='../images/profile/avatars/avatar". $i ."icon.png' />";
				$i++;
			};
		?>

		<iframe src="" width="0" height="0" frameborder="0"></iframe>
	</body>

	<script type="text/javascript">
		$(document).ready(function() {
			$("img").click(function() {
				var index = $("img").index(this) + 1;
				$("iframe").attr("src", "../scripts/avatarChangeScript.php?image=images/profile/avatars/avatar"+index+".png");
				setTimeout(function() {
						window.top.location.href = "http://8112achs.tk/profile.php";
					}, 300);
			});
		});
	</script>
</html>
