<!DOCTYPE html>
<html>
	<head>
		<title>Loading...</title>
		<link rel="stylesheet" type="text/css" href="css/sviAchievementi.css" />
		<script src="scripts/jquery-3.1.0.min.js"></script>
	</head>


	<body>
		<div id="sajt">
			<div id="topLimiter"></div>
			<?php include('scripts/loginStatus.php'); ?> <!--ORIGINAL (MOZDA CES MORATI DA OBRISES OVO ZATO STO PHP RADI KROZ COMMENT JA MSM)-->
			<?php include('elements/pointCounter.php'); ?>

			<div id="profile">
				<div id="topUserPoints"><?php echo $_SESSION['points']; ?> poena.</div>
				<img class="avatarImage" src="<?php echo $_SESSION['image']; ?>" />
				<img class="avatarFrame" src="<?php echo $_SESSION['frameImage']; ?>" />
				<div id="username"><?php echo $_SESSION['username']; ?></div>
			</div>


			<div id="menu">
				<div id="logout" class="mouseHover button">Izloguj se</div>
				<div id="back" class="mouseHover button">Nazad</div>
			</div>

			<div id="title">SVI ACHIEVEMENTI</div>

			<div id="topten">
				<?php include('elements/topten.php'); ?>
			</div>

			<div id="achievements">
				<div id="achievementPoints"><?php echo $_SESSION['totalPoints']; ?></div>
				<div id="achievementsFrame">
					<iframe src="elements/sviAchievementi.php" width="718" height="550" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	</body>

	<script type="text/javascript">
	//STAVLJA USERNAME I MENJA TITLE STRANICE
		$(document).ready(function() {
			$("title").html("Achievementi: <?php echo $_SESSION['username']; ?>");
			$("#achievementiButton").click(function() {
				$("#achievements").slideToggle();
			});
		});

	//LOGOUT
		$(document).ready(function() {
			$("#logout").click(function() {
				window.location = "scripts/logout.php";
			});
		});

	//NAZAD DUGME
		$(document).ready(function() {
			$("#back").click(function() {
				window.location = "profile.php";
			});
		});

	//DUGME
		$(document).ready(function() {
			$(".button").mouseup(function() {
				$(this).removeClass("buttonPressed");
			});
			$(".button").mouseleave(function() {
				$(this).removeClass("buttonPressed");
			});
			$(".button").mousedown(function() {
				$(this).addClass("buttonPressed");
			});
		});

	//USERNAME FONT SIZE SMANJI AKO IMA VISE OD 10 KARAKTERA
		$(document).ready(function() {
			var x = $("#username").html().length;
			if (x>10) {
				$("#username").css("font-size", "12px");
			};
		});

	//KADA KLIKNES NA PROFIL DRUGOG TIPA
		$(document).ready(function() {
			$(".toptenUserAvatarFrame").click(function() {
				var index = $(".toptenUserAvatarFrame").index(this);
				var username = $(".toptenUserUsername:eq("+index+")").html(); //ZA NEKI LATER USE
				window.location = "viewProfile.php?user=" + username;
			});
		});

	//PRVA DVA LIKA SU TOP TOP
		$(document).ready(function() {
			$(".topUser:eq(0)").css("width","464px").css("height","270px").css("margin-left","100px").css("margin-top","10px");
			$(".toptenUserAvatarImage:eq(0)").css("zoom","2");
			$(".toptenUserAvatarFrame:eq(0)").css("zoom","2");
			$(".toptenUserPoints:eq(0)").css("zoom","2");
			$(".toptenUserUsername:eq(0)").css("zoom","2");

			$(".topUser:eq(1)").css("width","348px").css("height","360px").css("margin-top","30px");
			$(".toptenUserAvatarImage:eq(1)").css("zoom","1.5");
			$(".toptenUserAvatarFrame:eq(1)").css("zoom","1.5");
			$(".toptenUserPoints:eq(1)").css("zoom","1.5");
			$(".toptenUserUsername:eq(1)").css("zoom","1.5");
		});
	</script>
</html>