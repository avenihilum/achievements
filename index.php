<!DOCTYPE html>
<html>
	<head>
		<title>Achievementi</title>
		<link rel="stylesheet" type="text/css" href="css/index.css" />
		<script src="scripts/jquery-3.1.0.min.js"></script>
	</head>
	
	<?php include('scripts/loginStatus.php'); ?> <!--ORIGINAL (MOZDA CES MORATI DA OBRISES OVO ZATO STO PHP RADI KROZ COMMENT JA MSM) -->

	<body>
		<p>Uloguj se</p>

		<div id="login">
			<form action='scripts/logIn.php' method='POST'>
				<input type='text' name='username' placeholder="Unesi korisnicko ime." />
				<input type='password' name='password' placeholder="Unesi lozinku." /><br />
				<input type='submit' value="Uloguj se" class="mouseHover" />
			</form>
		</div>

		<p class="mouseHover">Napravi nalog</p>

		<div id="createAccount">
			<form action="scripts/accountCreation.php" method="POST">
				<input type="text" name="username" placeholder="Unesi korisnicko ime ovde." /></br>
				<input type="password" name="password" placeholder="Unesi lozinku ovde." /></br>
				<input type="password" name="passwordCheck" placeholder="Unesi lozinku ovde." /></br>
				<input type="submit" value="Napravi nalog" class="mouseHover" />
			</form>
		</div>
	</body>

	<script type="text/javascript">
		$(document).ready(function() {
			$("p:eq(1)").click(function() {
				$("#createAccount").fadeToggle();
			});
		});
	</script>
</html>