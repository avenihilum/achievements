<!DOCTYPE html>
<html>
	<head>
		<title>Loading...</title>
		<link rel="stylesheet" type="text/css" href="css/profile.css" />
		<script src="scripts/jquery-3.1.0.min.js"></script>
	</head>


	<body>
		<div id="sajt">
			<div id="topLimiter"></div>
			<?php include('scripts/loginStatus.php'); ?> <!--ORIGINAL (MOZDA CES MORATI DA OBRISES OVO ZATO STO PHP RADI KROZ COMMENT JA MSM)-->
			<?php include('elements/pointCounter.php'); ?>

			<div id="profile">
				<div id="rank"></div>
				<img class="avatarImage" src="<?php echo $_SESSION['image']; ?>" />
				<img class="avatarFrame" src="<?php echo $_SESSION['frameImage']; ?>" />
				<div id="username"><?php echo $_SESSION['username']; ?></div>
			</div>

			<div id="menu">
				<div id="logout" class="mouseHover button">Izloguj se</div>
				<div id="changeAvatar" class="mouseHover button">Promeni avatar</div>
				<div id="sviAchievementi" class="mouseHover button">Svi achievementi</div>
			</div>

			<div id="title">ACHIEVEMENTI</div>

			<div id="progressBar">
				<img src="images/site/progressBar.png" />
				<div><?php echo $_SESSION['points']; ?>/<?php echo $_SESSION['totalPoints']; ?> poena</div>
				<progress value="<?php echo $_SESSION['points']; ?>" max="<?php echo $_SESSION['totalPoints']; ?>"></progress>
			</div>

			<div id="unlocker">
				<div class="delimiter"></div>
				<form target="achievementUnlock" action="scripts/unlocker.php" method="POST">
					<p>Ovde mozes uneti kod za otkljucavanje achievementa.</p>
					<input type="password" name="code" placeholder="Unesi kod ovde." style="width: 300px;" /><br />
					<input id="submit" class="mouseHover reactive button" type="submit" value="Unesi kod" />
				</form>
			</div>

			<div id="achievementiButton" class="button mouseHover">Achievementi</div>

			<div id="achievements" style="display:none">
				<div id="achievementPoints"><?php echo $_SESSION['points']; ?></div>
				<div id="achievementsFrame">
					<iframe src="elements/achievements.php" width="718" height="550" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	</body>

	<div id="achievementUnlockModal">
		<div id="achievementUnlockContent">
			<iframe id="modalIframe" src="scripts/unlocker.php" name="achievementUnlock" width="420" height="180" frameborder="0"></iframe>
		</div>
	</div>

	<div id="avatarChangeModal">
		<div id="avatarChangeContent">
			<iframe id="modalIframe" src="elements/avatarChange.php" name="avatarChange" width="634" height="316" frameborder="0"></iframe>
		</div>
	</div>

	<script type="text/javascript">
	//STAVLJA USERNAME I MENJA TITLE STRANICE
		$(document).ready(function() {
			$("title").html("Achievementi: <?php echo $_SESSION['username']; ?>");
			$("#achievementiButton").click(function() {
				$("#achievements").slideToggle();
			});
		});

	//LOGOUT
		$(document).ready(function() {
			$("#logout").click(function() {
				window.location = "scripts/logout.php";
			});
		});

	//AVATAR CHANGE
		$(document).ready(function() {
			$("#changeAvatar").click(function() {
				$("#avatarChangeModal").show();
			});
			$("#avatarChangeModal").click(function() {
				$(this).hide();
			});
		});

	//ALL ACHIEVEMENTS DUGME
		$(document).ready(function() {
			$("#sviAchievementi").click(function() {
				window.location = "sviAchievementi.php";
			});
		});

	//PALI #achievementUnlock KADA SUBMITUJE FORMU I VRACA NA PROFIL POSLE 3 SEKUNDE
		$(document).ready(function() {
			$("form").submit(function() {
				$(".reactive").attr("value","Otkljucavanje...");
				setTimeout(function() {
					$("#achievementUnlockModal").show();
					setTimeout(function() {
						location.reload();
					}, 3000);
				}, 1000);
			});
		});

	//DUGME
		$(document).ready(function() {
			$(".button").mouseup(function() {
				$(this).removeClass("buttonPressed");
			});
			$(".button").mouseleave(function() {
				$(this).removeClass("buttonPressed");
			});
			$(".button").mousedown(function() {
				$(this).addClass("buttonPressed");
			});
		});

	//RANKING (common, uncommon, rare, epic, legendary)

	ranking();
		function ranking() {
			var x = $("progress").attr("value");
			var epic = $("progress").attr("max")*0.95;
			var rare = $("progress").attr("max")*0.65;
			var uncommon = $("progress").attr("max")*0.30;
			var common = $("progress").attr("max")*0.1;
			var trash = 0;

			if (x>epic) {
				$("#rank").html("Legendary").css("color","orange");
			} else if (x>rare) {
				$("#rank").html("Epic").css("color","purple");
			} else if (x>uncommon) {
				$("#rank").html("Rare").css("color","blue");
			} else if (x>common) {
				$("#rank").html("Uncommon").css("color","green");
			} else if (x>trash) {
				$("#rank").html("Common").css("color","white");
			} else {
				$("#rank").html("Trash").css("color","grey");
			};
		};

	//USERNAME FONT SIZE SMANJI AKO IMA VISE OD 10 KARAKTERA
		$(document).ready(function() {
			var x = $("#username").html().length;
			if (x>10) {
				$("#username").css("font-size", "12px");
			};
		});
	</script>
</html>