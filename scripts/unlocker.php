<!DOCTYPE html>
<html>
	<head>
		<title>Loading...</title>
		<link rel="stylesheet" type="text/css" href="../css/unlocker.css">
		<script src="scripts/jquery-3.1.0.min.js"></script>
	</head>

	<body>
		<?php
			$code = htmlspecialchars($_POST['code']);
			include('../elements/connection.php');

			$achievementCheck = mysqli_query($connection, "SELECT * FROM admin");
			$achievementCheckData = mysqli_fetch_assoc($achievementCheck);

			if (strpos($achievementCheckData['codes'], "," .$code. ",") !== False) {

				session_start();
					$achievementFind = mysqli_query($connection, "SELECT * FROM achievements WHERE code='$code'");
					$achievementFindData = mysqli_fetch_assoc($achievementFind);

					$achievementId = $achievementFindData['id'];

					$id = $_SESSION['id'];

					$query = mysqli_query($connection, "SELECT achievements FROM users WHERE id='$id'");
					$data = mysqli_fetch_assoc($query);

					if (strpos($data['achievements'], "," .$achievementId. ",") !== False) {
				    	echo "<p class='poruka'>Vec imas taj achievement.</p>";
					} else {
						$achievement = $data['achievements']. "," .$achievementId. ",";
						mysqli_query($connection, "UPDATE users SET achievements='$achievement' WHERE id='$id'");

						$_SESSION['achievements'] = $achievement;

						echo "<div id='achievement'>
									<div id='levi'>
										<img src='". $achievementFindData['image'] ."'>
									</div>
									<div id='desni'>
										<div class='achievementName'>". $achievementFindData['name'] ."</div>
									</div>

									<div id='achievementResultBackground'><img src='../images/site/achievementUnlockShade.png' /></div>
							  </div>";
					}
				session_commit();

			} else {
				echo "<p class='poruka'>Uneo si pogresan kod.</p>";
			};
		?>
	</body>
</html>