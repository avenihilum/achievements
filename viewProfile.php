<!DOCTYPE html>
<html>
	<head>
		<title>Loading...</title>
		<link rel="stylesheet" type="text/css" href="css/viewAchievements.css" />
		<script src="scripts/jquery-3.1.0.min.js"></script>
	</head>


	<body>
		<div id="sajt">
			<div id="topLimiter"></div>
			<?php include('scripts/loginStatus.php'); ?> <!--ORIGINAL (MOZDA CES MORATI DA OBRISES OVO ZATO STO PHP RADI KROZ COMMENT JA MSM)-->
			<?php include('elements/pointCounter.php'); ?>
			<?php include('scripts/notLoggedRedirect.php'); ?>

			<div id="profile">
				<div id="topUserPoints"><?php echo $_SESSION['points']; ?> poena.</div>
				<img class="avatarImage" src="<?php echo $_SESSION['image']; ?>" />
				<img class="avatarFrame" src="<?php echo $_SESSION['frameImage']; ?>" />
				<div id="username"><?php echo $_SESSION['username']; ?></div>
			</div>

			<div id="menu">
				<div id="logout" class="mouseHover button">Izloguj se</div>
				<div id="back" class="mouseHover button">Nazad</div>
			</div>

			<div id="title"></div>

			<div id="topten">
				<?php include('elements/viewProfile.php'); ?>
			</div>

			<div id="achievements">
				<div id="achievementsFrame">
					<iframe src="elements/viewAchievements.php" width="718" height="550" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	</body>

	<script type="text/javascript">
	//STAVLJA USERNAME I MENJA TITLE STRANICE
		$(document).ready(function() {
			$("title").html("Achievementi: <?php echo $_SESSION['username']; ?>");
			$("#achievementiButton").click(function() {
				$("#achievements").slideToggle();
			});
		});

	//MENJA NASLOV SAJTA NA USERNAME IZABRANOG PROFILA
		$(document).ready(function() {
			var title = $(".toptenUserUsername").html();
			$("#title").html(title);
		});

	//LOGOUT
		$(document).ready(function() {
			$("#logout").click(function() {
				window.location = "scripts/logout.php";
			});
		});

	//DUGME
		$(document).ready(function() {
			$(".button").mouseup(function() {
				$(this).removeClass("buttonPressed");
			});
			$(".button").mouseleave(function() {
				$(this).removeClass("buttonPressed");
			});
			$(".button").mousedown(function() {
				$(this).addClass("buttonPressed");
			});
		});

	//USERNAME FONT SIZE SMANJI AKO IMA VISE OD 10 KARAKTERA
		$(document).ready(function() {
			var x = $("#username").html().length;
			if (x>10) {
				$("#username").css("font-size", "12px");
			};
		});

	//NAZAD DUGME
		$(document).ready(function() {
			$("#back").click(function() {
				window.location = "sviAchievementi.php";
			});
		});
	</script>
</html>