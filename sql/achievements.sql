-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2021 at 01:28 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `achievements`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `points` int(11) NOT NULL,
  `image` varchar(2000) NOT NULL DEFAULT '../images/achievements/placeholder.png',
  `code` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `name`, `description`, `points`, `image`, `code`) VALUES
(8, 'PermaDRUNK', 'Bio si pijan vise od 15h', 150, '../images/achievements/PermaDRUNK.png', 'CALEEEEEE'),
(9, 'Secret Handshake', 'Znas hausov secret passphrase', 50, '../images/achievements/SecretHandshake.png', 'banegejpapucar'),
(10, 'Death From Above', 'Kulturno si urinirao po ljuljaskama u kamenom sa visine vece od 2m.', 100, '../images/achievements/placeholder.png', 'Kutinetina'),
(11, 'Green Tea Purge', 'Popio si kadzus.', 75, '../images/achievements/placeholder.png', 'customgrow420'),
(12, 'Leap of Faith', 'Skocio si na zid komsije sa terase iz Hausa, pijan.', 200, '../images/achievements/placeholder.png', 'paralizaunajavi'),
(15, 'Salty Fisting', 'Drzao si kocku leda i soli u saci 1 min.', 100, 'http://vignette2.wikia.nocookie.net/agents-of-cia/images/2/20/8589130429320-bane-batman-mask-wallpaper-hd.jpg/revision/latest?cb=20140908111218', '420025');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `achievementids` varchar(2000) NOT NULL,
  `codes` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `achievementids`, `codes`) VALUES
(2, ',8,9,10,11,12,15,', ',CALEEEEEE,banegejpapucar,Kutinetina,customgrow420,paralizaunajavi,420025,');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` int(1) NOT NULL DEFAULT 1,
  `achievements` varchar(2000) NOT NULL,
  `image` varchar(2000) NOT NULL DEFAULT 'images/profile/avatars/avatar1.png',
  `frameImage` varchar(2000) NOT NULL DEFAULT 'images\\profile\\profileFrame.png',
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `type`, `achievements`, `image`, `frameImage`, `points`) VALUES
(20, 'Nemzax', '$2y$10$XuhF6JUm3Q6z6v.TzT6Tau07U0glFdajObplNshGErifE0uS8vZju', 0, ',8,,9,,10,', 'images/profile/avatars/avatar12.png', 'images/profile/profileFrame.png', 300),
(21, 'Duxan', '$2y$10$jq2Q1HXqbSVrqWeW7gXShu9Xyt2bgdMuIexPtmLYjYoSKXvjcjMO2', 1, ',9,,8,,10,', 'images/profile/avatars/avatar4.png', 'images/profile/profileFrame.png', 300),
(28, 'fish', '$2y$10$jk8UDk5BpN6fzkO7WGRUneYRNySN1wZ57k7U2tNb.Qt5ueODxuIou', 1, ',9,', 'images/profile/avatars/avatar6.png', 'images/profile/profileFrame.png', 50),
(30, 'Mracni', '$2y$10$G1ecOiQTHsj3VghE6TOtLO.BDJ0HmLtKsI/PT2OC/rwbEfDOjSj2a', 1, '', 'images/profile/avatars/avatar4.png', 'images/profile/profileFrame.png', 0),
(31, 'Sjeba', '$2y$10$sYNnwFjmSSvy3NOPAYhwvO2IrpVvng9ENI.nIyGpLL6owha/1nTvu', 1, ',8,,9,,12,', 'images/profile/avatars/avatar7.png', 'images/profile/profileFrame.png', 400),
(32, 'Cheecher', '$2y$10$DFC2jOvCaji7s2NgBBO9OekLGsWFYLrgN1weInHqudFzEuh8Z3J/2', 1, '', 'images/profile/avatars/avatar4.png', 'images/profile/profileFrame.png', 0),
(33, 'Aerosin', '$2y$10$uFz.c98T4LTzJewqLVDpe.6AXtu67OEolYGyiixifDplzj9PUdrlK', 1, ',9,,11,', 'images/profile/avatars/avatar9.png', 'images/profile/profileFrame.png', 125),
(34, 'SveSu', '$2y$10$FQGhuYu8u.jBQlaclVLAeObrs1zCASz6kJJaa8BTHISQKXjNnzvKW', 1, '', 'images/profile/avatars/avatar1.png', 'images\\profile\\profileFrame.png', 0),
(35, 'Kurve', '$2y$10$euus4uzIbr/fvIixK31E2OWBb4dqcKy54wnZDcOzQxmSUwTMBSbdu', 1, '', 'images/profile/avatars/avatar1.png', 'images\\profile\\profileFrame.png', 0),
(36, 'bandzas', '$2y$10$.fCBMz9Y9eAbjVFfT0wjOe59ptOgJaQUAxDNzN8.bxAaD2BvhoeRC', 1, ',9,,15,', 'images/profile/banaAvatar.png', 'images/profile/profileFrame.png', 150),
(37, 'jebac', '$2y$10$1VmbwkmbEmZwlNV.MGJzH.Lv35GXMIdrACQ39CK0MQwnBjpBAFlc6', 1, ',9,', 'images/profile/avatars/avatar3.png', 'images/profile/profileFrame.png', 50);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achievements`
--
ALTER TABLE `achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
